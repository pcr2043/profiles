<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $table= 'profiles';
   
    protected $guarded= ['name'];
    protected $fillable= [];

    protected $appends = ['name'];

    public function getNameAttribute(){
        return $this->first_name.' '.$this->last_name;
    }
}
