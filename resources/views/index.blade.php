<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel</title>

    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

</head>

<body>

    <div class="container justify-center">
        <h1 class="mobile-100 text-center" onclick="edit=!edit">Profile Browser</h1>
    </div>
    <div class="container profile-container">

        <ul class="profiles">
            @foreach($profiles as $index => $profile)
            <li class="{{ $index == 0 ? 'active' : '' }}" data-toggle="collapse"
                data-target="profile-{{ $profile->id }}">
                <div class="item">
                    {{ $profile->name }}
                    <img class="delete" onclick="framework.submit(null, null, '{{ '/profile/'.$profile->id }}')"
                        src="https://cdn.iconscout.com/icon/free/png-256/delete-737-475058.png" alt="delete">
                </div>
                <div class="profile" id="profile-{{ $profile->id }}">
                    <div data-if="!edit">
                        <img src="{{ $profile->image }}" />
                        <h1>{{ $profile->name }}</h1>
                        <span>{{ $profile->description }}</span>

                    </div>
                    <div class="edit" data-if="edit">
                        @include('partials/profile', ['endpoint' => '/profile/'.$profile->id, 'verb'=> 'put' , 'model'
                        => $profile])
                    </div>
                </div>
            </li>
            @endforeach
            <li data-if="edit" data-toggle="collapse" data-target="new">
                <div class="item new">
                    + Click to add NEW
                </div>
                <div class="profile" id="new">
                    <div class="edit" data-if="edit">
                        @include('partials/profile', ['endpoint' => '/profile/', 'verb'=> 'post', 'model' =>
                        null])
                    </div>
                </div>
            </li>
        </ul>

</body>

<script src="{{ mix('js/app.js') }}"></script>
</html>