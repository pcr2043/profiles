<form data-endpoint="{{ $endpoint }}" data-verb="{{ $verb}}" onsubmit="return framework.submit(this, null, null)">
    <input name="image" type="text" value="{{ isset($model->image) ? $model->image : ''  }}" placeholder="enter image url" required />
  <input name="first_name" type="text" value="{{ isset($model->first_name) ? $model->first_name : ''  }}" placeholder="enter first name" required />
  <input name="last_name" type="text" value="{{ isset($model->last_name) ? $model->last_name : '' }}" placeholder="enter last name" required />
  <textarea name="description" rows="10" placeholder="enter the bio" required>{{ isset($model->description) ? $model->description : ''  }}</textarea>
  <button type="submit" class="primary float-right">SAVE</button>
</form>