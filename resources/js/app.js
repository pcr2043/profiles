
edit = false;



window.framework = {
  // attribute modifier function to collapse a element base on expression passed thougth data-id
  dataIf: function () {
    // set element containing data-if expression an evalutes it
    document.querySelectorAll('[data-if]').forEach(function (el) {

      let expression = eval(el.getAttribute('data-if'));


      // if expression is false
      if (!expression) {
        //save current display
        el.setAttribute('data-display', el.style.display)
        //set display to none
        el.style.display = 'none';
      }
      else
        el.style.display = (el.getAttribute('data-display') != null) ? el.getAttribute('data-display') : 'block'


    })
  },
  events() {

    // get all with onclick
    document.querySelectorAll('[onclick]').forEach(function (el) {

      // add listner for all element with onclick, evaludate expression and apply doom changes base on attributes modiers
      el.addEventListener("click", function (ev) {
        // get expression
        var expression = ev.target.getAttribute('onclick');


        // evalute
        eval.call(expression)

        // apply changes
        // call attribute modifiers to evaluate new expressions
        framework.dataIf();
      });



    })

  },
  init: function () {
    this.events();
    this.dataIf()
  }
}

//init framework features
framework.init();


framework.submit = function (form, id, deleteEndpoint) {

  var token = document.head.querySelector('meta[name="csrf-token"]');

  //create XMLHttpRequest request with response json
  var xhr = new XMLHttpRequest();
  xhr.responseType = 'json';

  //create a far for data
  var data = new FormData();

  //create a var for endpoint
  var endpoint = '/'

  //create the http verb, default is get
  var verb = 'get'

  //if is a form fill data from form
  if (form != null) {
    //get data from form
    data = new FormData(form);

    //get endpoint
    endpoint = form.getAttribute('data-endpoint')

    //get verb
    verb = form.getAttribute('data-verb');

  }

  //checkf if deleteID is set wich means request is to delete
  if (deleteEndpoint !== null) {
    endpoint = deleteEndpoint
    verb = 'delete';
  }

  //set default xhr verb to post then override
  xhr.open('post', 'https://serene-falls-94329.herokuapp.com/' + endpoint);

  //because xhr verb does not work well i have to override it
  data.append('_method', verb);

  //set token for laravel and X-Requested-With to XMLHttpRequest
  xhr.setRequestHeader('X-CSRF-TOKEN', token.content);
  xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");


  //send request
  xhr.send(data);


  xhr.onreadystatechange = function () {

    if (xhr.readyState == 4) {
      switch (xhr.status) {
        case 200:

          if (verb == 'put') {

            localStorage.setItem('profile-id', xhr.response.id)
            window.location.reload();
          }

          if (verb == 'delete') {
            localStorage.removeItem('profile-id')
            window.location.reload();
          }

        case 201:

          if (verb == 'post') {
            localStorage.setItem('profile-id', xhr.response.id)
            window.location.reload();
            // window.location.reload();
          }

        case 405:
          console.log(405)

        case 422:
          console.log(422)
      }
    }

  }

  return false;
}


//set collapse function
document.querySelectorAll('[data-toggle="collapse"]').forEach(function (clickingElement) {


  clickingElement.addEventListener('click', function () {

    //get the id of the element to be toggle
    let toggleElementID = this.getAttribute('data-target');

    //save current selected profile for page reload
    localStorage.setItem('profile-id', toggleElementID.replace('profile-', ''))

    //access dom for this element
    let toggleElement = document.getElementById(toggleElementID);
    console.log(toggleElement)
    // we want ot maintain one tab active like the UI Demo

    // so we have to remove the current active
    // but only if is clicked element if different from selected
    // so lets check if someting is selected

    // find the li active
    let activeElement = toggleElement.parentElement.parentElement.querySelector('.active')


    //to remove class active, activeElement should exist and to toggleElement should be diferent than activeElement
    let validationforToggle = activeElement != null && toggleElement.getAttribute("id") != activeElement.getAttribute("id")

    // this is true we can remove active class from activeElement
    if (validationforToggle)
      activeElement.setAttribute("class", activeElement.getAttribute("class").replace("active", ""));

    // to add class active activeElement should null or should be different
    if (activeElement == null || validationforToggle)
      toggleElement.parentElement.setAttribute("class", toggleElement.parentElement.getAttribute("class") + ' active');







  })
})


//check current profile
let id = localStorage.getItem('profile-id');
if (id != null) {
  document.querySelector('[data-target=profile-' + id + ']').click()
}




//some magin script to activate edition
let magicWord = 'editme';
window.currentMagic = ''

let timeOutMagic = null;
document.addEventListener('keydown', function (e) {


  window.currentMagic += e.key;

  for (var i = 0; i < currentMagic.length; i++) {
    if (currentMagic[i] != magicWord[i])
      currentMagic = '';
  }

  console.log(currentMagic)
  if (currentMagic == magicWord) {
    currentMagic = '';
    edit = !edit
    localStorage.setItem("profile-edit", edit);
    framework.dataIf()
  }


});

//check if mode edit and set
var mode = localStorage.getItem("profile-edit");
if (mode != null) {
  edit = eval(mode)
  framework.dataIf()
}